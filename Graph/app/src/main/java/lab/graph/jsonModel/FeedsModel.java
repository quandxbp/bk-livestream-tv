package lab.graph.jsonModel;

import com.google.gson.annotations.SerializedName;

public class FeedsModel {

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("entry_id")
    public int entry_id;

    @SerializedName("field2")
    public int field2;
    @SerializedName("field3")
    public int field3;

}
