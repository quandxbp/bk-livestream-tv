package lab.graph.AsyncTask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.loopj.android.http.HttpGet;

import java.io.ByteArrayOutputStream;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class AndroidAsyncTask extends AsyncTask<String, String, String> {

    private Context mContext;
    private WebListener mListener;

    public AndroidAsyncTask(Context context, WebListener listener) {
        mContext = context;
        mListener = listener;
    }

    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected void onPostExecute(String path) {
        if (mListener == null) {
            return;
        }
        Log.d("Path :", path);
        if (path.length() > 0) {

            mListener.onStreamResponse(path);
        } else {
            mListener.onStreamError(path);
        }
    }

    @Override
    protected String doInBackground(String... urls) {
        String url = urls[0];
        String responseString = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(url);
            HttpResponse response = httpclient.execute(httpget);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response.getEntity().writeTo(out);
            responseString = out.toString();
            out.close();
            return responseString;
        } catch (Exception e) {
            Log.e("Exception :", "Err " + e);
        }

        return responseString;
    }

    public interface WebListener {
        void onStreamResponse(String link);

        void onStreamError(String message);
    }
}
