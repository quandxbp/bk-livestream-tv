package lab.livestreamapp;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import lab.livestreamapp.beans.RowItem;

public class MainActivity extends Activity {
    public ImageButton openMic;

    public TextToSpeech tts;

    public ListView listView;
    List<RowItem> rowListItems;

    // Get set(list) of the key (Ex : ["VTV1", "VTV2", "VTV3"] )
    Set<String> keys = Constants.CHANNEL_DICT.keySet();
    // Convert set to String array
    final String[] channelList = keys.toArray(new String[keys.size()]);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Config Text to Speech
        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.US);
                }
            }
        });

        // Config Open Micro Button
        openMic = findViewById(R.id.speakerButton);

        openMic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnToOpenMic();
            }
        });

        rowListItems = new ArrayList<>();
        for (int i = 0; i < channelList.length; i++) {
            // Set display Image for the channel
            int disPlayImg;
            if (Constants.IMAGE_DICT.get(channelList[i]) == null )
                disPlayImg = Constants.NO_IMG;
            else
                disPlayImg = Constants.IMAGE_DICT.get(channelList[i]);
            // Set display channel name
            String displayChannel = "Kênh " + channelList[i];
            // Set display description for every channel
            String displayDesc = Constants.DESC_DICT.get(channelList[i]);
            // Create each row object
            RowItem item = new RowItem(disPlayImg, displayChannel, displayDesc);
            // Add each row into rowListItems
            rowListItems.add(item);
        }

        listView = findViewById(R.id.ListView);

        CustomListViewAdapter adapter = new CustomListViewAdapter(this,
                R.layout.list_item, rowListItems);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                Intent myIntent = new Intent(view.getContext(), VideoDisplay.class);

                // Put extra data into the new activity
                myIntent.putExtra("CHANNEL", String.valueOf(pos));
                startActivity(myIntent);
            }
        });
    }

    private void btnToOpenMic() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "vi-VN");
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Hi Speak Now ...");

        try {
            startActivityForResult(intent,Constants.REQ_CODE_SPEECH_OUTPUT);
        }
        catch (ActivityNotFoundException err) {
            Log.d(Constants.MYTAG, "Activity Not Found :" + err);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Constants.REQ_CODE_SPEECH_OUTPUT: {
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> voiceIntent = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String receiveText = voiceIntent.get(0).toLowerCase();

//                    Log.d(Constants.MYTAG,receiveText);

                    TextProcess textProc = new TextProcess();

                    String receiveChannel = textProc.processVoice(receiveText, tts);

                    Log.d(Constants.MYTAG,receiveChannel);

                    if (receiveChannel.equals("password")) {
                        Intent myIntent = new Intent(getApplicationContext(), VideoDisplay.class);
                        // Put extra data into the new activity
                        myIntent.putExtra("CHANNEL", String.valueOf("secret"));
                        startActivity(myIntent);
                    }
                    else if(!receiveChannel.equals("false")) {
                        Intent myIntent = new Intent(getApplicationContext(), VideoDisplay.class);
                        // Put extra data into the new activity
                        myIntent.putExtra("CHANNEL", String.valueOf(receiveChannel));
                        startActivity(myIntent);
                    }
                    else {
                        tts.speak("Please try another channel !", TextToSpeech.QUEUE_ADD, null);
                    }

                }
            }
            break;
        }
    }
}
