package lab.livestreamapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;


public class VideoDisplay extends AppCompatActivity implements AndroidAsyncTask.VideoStreamingListener{
    public VideoView videoView;
    public String Path = "";
    public MediaController mediaController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_display_video);

        Intent intent = getIntent();

        videoView = findViewById(R.id.VideoView);

        // Get position of channel when user click to the list
        String channel = intent.getStringExtra("CHANNEL");

        if (channel.equals("secret")) {
            Path = String.valueOf("http://123.29.128.167/v5live.m3u8?c=vstv074&q=medium&deviceType=7&userId=5&deviceId=9822771&type=tv&rkey=030617023zaqsxnr&p=0&zn=netott&ctl=10.108&sl=6&drmsrv=drmv.mytvnet.vn&pkg=pkg11&profiles=high,medium&location=NA&rbk=20316270137&pnode=172.16.52.133");
        }
        // Channel length is greater than 1 ( Ex: VTV1, VTV2)
        else if(channel.length() > 1) {
            Path = String.valueOf(Constants.CHANNEL_DICT.get(channel.toUpperCase()));
        }
        // else Channel length is equal to 1 ( Ex: 1,2,3)
        else {
            // Set path(link) is the live stream link at position defined in dictionary
            Path = String.valueOf(Constants.CHANNEL_DICT.values().toArray()[Integer.parseInt(channel)]);
        }

        mediaController = new MediaController(VideoDisplay.this);
        mediaController.setAnchorView(videoView);

        new AndroidAsyncTask(this,this).execute(Path);

    }

    @Override
    protected void onResume() {
        super.onResume();

        // Restart live stream when click the home button
        videoView.start();
    }

    @Override
    public void onStreamResponse(String link) {
        videoView.setVideoPath(link);
        videoView.setMediaController(mediaController);

        videoView.start();
    }

    @Override
    public void onStreamError(String message) {

    }
}
