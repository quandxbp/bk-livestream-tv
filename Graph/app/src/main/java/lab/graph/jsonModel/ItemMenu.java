package lab.graph.jsonModel;

import java.io.Serializable;

public class ItemMenu implements Serializable {
    private String image;
    private String title;
    private String description;

    public ItemMenu(String title, String description){
        this.title = title;
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
