package com.example.ledblinky;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.hardware.usb.UsbManager;
import android.media.ExifInterface;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ledblinky.MVVM.VM.NPNHomeViewModel;
import com.example.ledblinky.MVVM.View.NPNHomeView;
import com.example.ledblinky.Network.ApiResponseListener;
import com.example.ledblinky.Network.VolleyRemoteApiClient;
import com.github.mjdev.libaums.UsbMassStorageDevice;
import com.github.mjdev.libaums.fs.FileSystem;
import com.github.mjdev.libaums.fs.UsbFile;
import com.github.mjdev.libaums.fs.UsbFileStreamFactory;
import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManager;
import com.google.android.things.pio.SpiDevice;
import com.google.android.things.pio.UartDevice;
import com.google.android.things.pio.UartDeviceCallback;
//import com.squareup.okhttp.Callback;
//import com.squareup.okhttp.OkHttpClient;
//import com.squareup.okhttp.Request;
//import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Skeleton of an Android Things activity.
 * <p>
 * Android Things peripheral APIs are accessible through the class
 * PeripheralManagerService. For example, the snippet below will open a GPIO pin and
 * set it to HIGH:
 * <p>
 * <pre>{@code
 * PeripheralManagerService service = new PeripheralManagerService();
 * mLedGpio = service.openGpio("BCM6");
 * mLedGpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
 * mLedGpio.setValue(true);
 * }</pre>
 * <p>
 * For more complex peripherals, look for an existing user-space driver, or implement one if none
 * is available.
 *
 * @see <a href="https://github.com/androidthings/contrib-drivers#readme">https://github.com/androidthings/contrib-drivers#readme</a>
 */
public class MainActivity extends Activity implements NPNHomeView{

    private static final String TAG = "BKIT";

    // UART Configuration Parameters
    private static final int BAUD_RATE = 115200;
    private static final int DATA_BITS = 8;
    private static final int STOP_BITS = 1;
    private UartDevice mUartDevice;
    private  int CHUNK_SIZE = 512;

    private ArrayList<Integer> sendValue = new ArrayList<>();
    int count = 0;


    //Timer for periodic uploading data
    Timer mBlinkyTimer;

    //An instance for network access
    NPNHomeViewModel mHomeViewModel;

    //Display IP Address of the device
    TextView txtIPAddress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        txtIPAddress = findViewById(R.id.txtIPAddress);
        final Context context= this;

        //An instance for network access
        mHomeViewModel = new NPNHomeViewModel();
        mHomeViewModel.attach(this, this);


        initUart();
        setupBlinkyTimer();


    }


    @Override
    public void onSuccessUpdateServer(String message) {
        //txtConsole.setText("Request server is successful");
        Log.d(TAG, "Request server is successful");
    }

    @Override
    public void onErrorUpdateServer(String message) {

        Log.d(TAG, "Request server is fail");
    }


    private void sendDataToThingSpeak(ArrayList<Integer> values){
        String temperature = Integer.toString(values.get(0));
        String lightLevel = Integer.toString(values.get(1));

        OkHttpClient okHttpClient = new OkHttpClient();
        Request.Builder builder = new Request.Builder();
        String API_KEY = "5FLGY7IM3R9FFYTD";
        String API_URL = "https://api.thingspeak.com/update?api_key=" + API_KEY + "&field2=" + temperature + "&field3=" + lightLevel;
        Request request = builder.url(API_URL).build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(TAG, "Exception : " + e);
            }

            @Override
            public void onResponse(Call call, Response response) {
                Log.d(TAG, "Response is : " + response);
            }
        });
    }

    private void updateNetworkStatus(){
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String display = "";

                if(Ultis.checkWifiConnected(MainActivity.this) == true)
                {
                    display += " Wifi connected: " + Ultis.getWifiIPAddress(MainActivity.this);
                }
                else if(Ultis.checkLanConnected(MainActivity.this) == true){
                    display = " Ethernet connected: " + Ultis.getIPAddress(true) ;
                }
                else
                {
                    display = " No connection";
                }

                txtIPAddress.setText(display);

            }
        });
    }


    private void setupBlinkyTimer()
    {
        mBlinkyTimer = new Timer();
        TimerTask blinkyTask = new TimerTask() {
            @Override
            public void run() {
//                sendDataToThingSpeak(23);
            }
        };

        mBlinkyTimer.schedule(blinkyTask, 60000, 60000);
    }




    private void initUart()
    {
        try {
            openUart("UART0", BAUD_RATE);
            Log.d(TAG, "Init uart successful");
        }catch (IOException e) {
            Log.d(TAG, "Error on UART API");
        }
    }


    public void writeUartData(UartDevice uart) {
        try {
            byte[] buffer = {'t'};
            int count = uart.write(buffer, buffer.length);
            Log.d(TAG, "Wrote " + count + " bytes to peripheral");
        }catch (IOException e)
        {
            Log.d(TAG, "Error on UART");
        }
    }

    /**
     * Callback invoked when UART receives new incoming data.
     */
    private UartDeviceCallback mCallback = new UartDeviceCallback() {
        @Override
        public boolean onUartDeviceDataAvailable(UartDevice uart) {
           //read data from Rx buffer
            try {
                byte[] buffer = new byte[CHUNK_SIZE];
                int noBytes = -1;
                String strData = "";
                while ((noBytes = mUartDevice.read(buffer, buffer.length)) > 0) {
                    Log.d(TAG,"Number of bytes: " + Integer.toString(noBytes));

                    strData = new String(buffer,0,noBytes, "UTF-8");

                    Log.d(TAG,"Value is : " + strData);

                    sendValue.add(Integer.parseInt(strData));

                    if (count % 2 == 1) {
                        sendDataToThingSpeak(sendValue);
                        sendValue.clear();
                        count = 0;
                    } else {
                        count += 1;
                    }


                }

            } catch (IOException e) {
                Log.w(TAG, "Unable to transfer data over UART", e);
                return false;
            }
            return true;
        }

        @Override
        public void onUartDeviceError(UartDevice uart, int error) {
            Log.w(TAG, uart + ": Error event " + error);
        }
    };

    private void openUart(String name, int baudRate) throws IOException {

        Log.d(TAG,PeripheralManager.getInstance().getUartDeviceList().toString() );
        List<String> uartList = PeripheralManager.getInstance().getUartDeviceList();
        for(int i = 0; i < uartList.size(); i ++){
            if(uartList.get(i).toLowerCase().indexOf("usb") >=0){
                name = uartList.get(i);
                break;
            }
        }

        mUartDevice = PeripheralManager.getInstance().openUartDevice(name);

        // Configure the UART
        mUartDevice.setBaudrate(baudRate);
        mUartDevice.setDataSize(DATA_BITS);
        mUartDevice.setParity(UartDevice.PARITY_NONE);
        mUartDevice.setStopBits(STOP_BITS);

        mUartDevice.registerUartDeviceCallback(mCallback);
    }

    private void closeUart() throws IOException {
        if (mUartDevice != null) {
            mUartDevice.unregisterUartDeviceCallback(mCallback);
            try {
                mUartDevice.close();
            } finally {
                mUartDevice = null;
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Attempt to close the UART device
        try {
            closeUart();
            mUartDevice.unregisterUartDeviceCallback(mCallback);

        } catch (IOException e) {
            Log.e(TAG, "Error closing UART device:", e);
        }
    }
}

