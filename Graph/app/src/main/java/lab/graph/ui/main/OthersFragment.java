package lab.graph.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.List;

import lab.graph.AsyncTask.JsonAsyncTask;
import lab.graph.R;
import lab.graph.jsonModel.FeedsModel;

public class OthersFragment extends Fragment  implements JsonAsyncTask.JsonListener{


    GraphView graph;
    Handler handler;

    Runnable r;

    Context jsonContext = this.getContext();
    JsonAsyncTask.JsonListener jsonListener = this;
    TextView currentLight;
    public OthersFragment() {
        Log.d("HCMUT", "Hello");
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_others, container, false);
        graph = view.findViewById(R.id.graph);
        //Create Async task to get light each 5s
        handler = new Handler();
        r = new Runnable() {
            public void run() {

                new JsonAsyncTask(jsonContext, jsonListener).execute("https://api.thingspeak.com/channels/728954/feeds.json?api_key=3EMA3MGNHN1H56GK&results=5&fbclid=IwAR3TwbuUJyakF3FW3c0ZQaexB-zp59nJ-AnMvWwkZyv9oCAw2XrSO0hBX3Q");

                handler.postDelayed(r, 5000);
            }
        };
        currentLight = view.findViewById(R.id.current_light);
        r.run();
        return view;
    }


    @Override
    public void onStreamResponse(List<FeedsModel> feeds) {
        Log.d("HCMUT", "Others: " + feeds.get(4).field3);
        Log.d("HCMUT", "others id: " + feeds.get(4).entry_id);
        graph.removeAllSeries();
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(feeds.get(0).entry_id, feeds.get(0).field3),
                new DataPoint(feeds.get(1).entry_id, feeds.get(1).field3),
                new DataPoint(feeds.get(2).entry_id, feeds.get(2).field3),
                new DataPoint(feeds.get(3).entry_id, feeds.get(3).field3),
                new DataPoint(feeds.get(4).entry_id, feeds.get(4).field3)

        });
        series.setColor(R.color.colorLine);
        currentLight.setText("Light now is: " + feeds.get(4).field3);
        graph.addSeries(series);
    }

    @Override
    public void onStreamError(List<FeedsModel> feeds) {
        Log.d("HCMUT", "Error: " + feeds);
    }
}
