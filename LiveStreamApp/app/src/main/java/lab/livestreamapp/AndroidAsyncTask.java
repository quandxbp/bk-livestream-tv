package lab.livestreamapp;

import android.content.Context;
        import android.os.AsyncTask;
        import android.util.Log;

        import com.google.gson.Gson;
        import com.loopj.android.http.HttpGet;

        import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;

        import cz.msebera.android.httpclient.HttpEntity;
        import cz.msebera.android.httpclient.HttpResponse;
        import cz.msebera.android.httpclient.client.HttpClient;
        import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class AndroidAsyncTask extends AsyncTask<String, String, String> {
    private Context mContext;
    private VideoStreamingListener mListener;

    public AndroidAsyncTask(Context context, VideoStreamingListener listener) {
        mContext = context;
        mListener = listener;
    }

    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected void onPostExecute(String path) {
        if (mListener == null) {
            return;
        }
        Log.d("Path :", path);
        if (path.length() > 0) {

            mListener.onStreamResponse(path);
        } else {
            mListener.onStreamError(path);
        }
    }


    public interface VideoStreamingListener {
        void onStreamResponse(String link);

        void onStreamError(String message);
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    @Override
    protected String doInBackground(String... strings) {
        String url = strings[0];

        try {
            if (url.contains("m3u8") || url.contains("porn")) {
                Log.d("HCMUT", url);
                return url;
            }
            else {
                Log.d("HCMUT", "MEEEEEEEEEÊ");
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet(url);
                HttpResponse response = httpclient.execute(httpget);
                HttpEntity entity = response.getEntity();

                if (entity != null) {
                    InputStream instream = entity.getContent();
                    String firstResponse = convertStreamToString(instream);
                    LinkModel objLink = new Gson().fromJson(firstResponse, LinkModel.class);
                    instream.close();
                    Log.d("HCMUT", objLink.getLow_res());
                    return objLink.getLow_res();
                }
            }

        } catch (Exception e) {
            Log.e("Exception :", "Err " + e);
        }

        return "";
    }
}
