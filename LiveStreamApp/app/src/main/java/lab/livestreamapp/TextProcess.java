package lab.livestreamapp;

import android.speech.tts.TextToSpeech;
import android.util.Log;


public class TextProcess {

    public String processVoice(String text,TextToSpeech tts) {
        if (text.equals("bạn anh quân") || text.equals("hello")) {
            tts.setPitch(0.5f);
            tts.speak("Xin chào mấy đứa", TextToSpeech.QUEUE_ADD, null);
            return "password";
        } else {
            String[] splitText = text.split(" ");
            String channel = "";

            for (String word : splitText) {
                if (Constants.CHANNEL_DICT.get(word.toUpperCase()) != null) {
                    channel = word;
                    break;
                }
            }

            if (channel.equals("")) {
                return "false";
            }

            return channel;
        }
    }



}
