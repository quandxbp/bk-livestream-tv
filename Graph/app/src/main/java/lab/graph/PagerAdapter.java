package lab.graph;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import lab.graph.ui.main.OthersFragment;
import lab.graph.ui.main.TemperatureFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    PagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }
    @Override
    public Fragment getItem(int position) {
        Fragment frag=null;
        switch (position){
            case 0:
                frag = new TemperatureFragment();
                break;
            case 1:
                frag = new OthersFragment();
                break;
        }
        return frag;
    }

    @Override
    public int getCount() {
        return 2;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0:
                title = "Temperature";
                break;
            case 1:
                title = "Light Level";
                break;
        }
        return title;
    }
}

