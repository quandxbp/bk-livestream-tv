package lab.livestreamapp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class Constants {
    public static final String MYTAG = "BKTV";
    public static final int REQ_CODE_SPEECH_OUTPUT = 143;
    
    public static final LinkedHashMap<String, String>  CHANNEL_DICT = new LinkedHashMap<String, String>(){{
        // VTV channels
        put("VTV1", "https://binggodeals.com/channel/vtv1");
        put("VTV2", "http://liverestreamobj.e66534d0.viettel-cdn.vn/hls/VTV2_HD/02.m3u8");
        put("VTV3", "http://liverestreamobj.e66534d0.viettel-cdn.vn/hls/VTV3_HD/02.m3u8");
        put("VTV4", "https://hjyjrvmlsk.vcdn.com.vn/hls/hktebdo/index.m3u8");
        put("VTV5", "http://liverestreamobj.e66534d0.viettel-cdn.vn/hls/VTV5_HD/02.m3u8");
        put("VTV6", "http://liverestreamobj.e66534d0.viettel-cdn.vn/hls/VTV6_HD/02.m3u8");
        put("VTV7", "http://liverestreamobj.e66534d0.viettel-cdn.vn/hls/VTV7_HD/02.m3u8");
        put("VTV8", "http://liverestreamobj.e66534d0.viettel-cdn.vn/hls/VTV8_HD/02.m3u8");
        put("VTV9", "http://liverestream2obj.5b1df984.cdnviet.com/hls/VTV9_HD_NEW/03.m3u8");

        // VTV Cab
        put("VTVcab1", "http://liverestreamobj.5b1df984.cdnviet.com/hls/GIAITRI_TV_HD/04.m3u8");
        put("VTVcab2", "http://liverestreamobj.5b1df984.cdnviet.com/hls/PHIM_VIET/04.m3u8");
        put("VTVcab4", "http://liverestreamobj.5b1df984.cdnviet.com/hls/VAN_HOA/04.m3u8");
        put("VTVcab5", "http://liverestreamobj.5b1df984.cdnviet.com/hls/E_CHANNEL/03.m3u8");
        put("VTVcab6_", "http://liverestreamobj.5b1df984.cdnviet.com/hls/HAY_TV/04.m3u8");
        put("VTVcab7", "http://liverestreamobj.5b1df984.cdnviet.com/hls/D_DRAMAS_HD/04.m3u8");
        put("VTVcab8", "http://liverestreamobj.5b1df984.cdnviet.com/hls/BIBI/03.m3u8");
        put("VTVcab9", "http://liverestream2obj.5b1df984.cdnviet.com/hls/INFO_TV_NEW/01.m3u8");
        put("VTVcab10", "http://liverestream2obj.5b1df984.cdnviet.com/hls/O2_TV_NEW/03.m3u8");
        put("VTVcab12", "http://liverestreamobj.5b1df984.cdnviet.com/hls/STYLE_TV_HD/04.m3u8");
        put("VTVcab15", "http://liverestreamobj.5b1df984.cdnviet.com/hls/M_CHANNEL/04.m3u8");
        put("VTVcab17", "http://liverestreamobj.5b1df984.cdnviet.com/hls/YEAH1_TV/02.m3u8");
        put("VTVcab19", "http://liverestreamobj.5b1df984.cdnviet.com/hls/FILM_TV/04.m3u8");
        put("VTVcab20", "http://liverestreamobj.5b1df984.cdnviet.com/hls/VFAMILY/02.m3u8");
        put("VTVcab21", "http://liverestreamobj.5b1df984.cdnviet.com/hls/BIBI_PLUS_clear/05.m3u8");
        put("VTVcab22", "http://liverestreamobj.5b1df984.cdnviet.com/hls/LIFE_TV/02.m3u8");

        // VTC
        put("VTC1", "https://cdn.vtc.gov.vn/live/eds/VTC1_1/hlsv5/VTC1_1-audio_20000=212400-video=2821200.m3u8");
        put("VTC2", "https://cdn.vtc.gov.vn/live/eds/VTC2_2/hlsv5/VTC2_2-audio_20000=212400-video=1275600.m3u8");
        put("VTC3", "http://liverestream2obj.5b1df984.cdnviet.com/hls/dvr/TC3/03-timeshifting.m3u8");
        put("VTC4", "https://cdn.vtc.gov.vn/live/eds/VTC4_4/hlsv5/VTC4_4-audio_20000=212400-video=1275600.m3u8");
        put("VTC5", "https://cdn.vtc.gov.vn/live/eds/VTC5_5/hlsv5/VTC5_5-audio_20000=212400-video=1275600.m3u8");
        put("VTC6", "https://cdn.vtc.gov.vn/live/eds/VTC6_6/hlsv5/VTC6_6-audio_20000=212400-video=1275600.m3u8");
        put("VTC7", "https://cdn.vtc.gov.vn/live/eds/VTC7_7/hlsv5/VTC7_7-audio_20000=212400-video=1200000.m3u8");
        put("VTC8", "https://cdn.vtc.gov.vn/live/eds/VTC8_8/hlsv5/VTC8_8-audio_20000=212400-video=1275600.m3u8");
        put("VTC9", "http://liverestream2obj.5b1df984.cdnviet.com/hls/TC9/03.m3u8");
        put("VTC10", "https://cdn.vtc.gov.vn/live/eds/VTC10_10/hlsv5/VTC10_10-audio_20000=212400-video=1275600.m3u8");
        put("VTC11", "https://cdn.vtc.gov.vn/live/eds/VTC11_11/hlsv5/VTC11_11-audio_20000=212400-video=1275600.m3u8");
        put("VTC13", "http://liverestream2obj.5b1df984.cdnviet.com/hls/TC13/03.m3u8");
        put("VTC14", "http://liverestream2obj.5b1df984.cdnviet.com/hls/TC14/03.m3u8");
        put("VTC16", "https://cdn.vtc.gov.vn/live/eds/VTC16_16/hlsv5/VTC16_16-audio_20000=212400-video=1275600.m3u8");
        put("Vietnam Journey HD", "http://vncdn.mediatech.vn/vovlive/tv1live.m3u8");

        // HTV
        put("HTV1", "http://live.cdn.mobifonetv.vn/motv/myhtv1_hls.smil/chunklist_b1200000.m3u8");
        put("HTV2", "http://live.cdn.mobifonetv.vn/motv/myhtv2_hls.smil/chunklist_b1200000.m3u8");
        put("HTV3", "http://live.cdn.mobifonetv.vn/motv/myhtv3_hls.smil/chunklist_b1200000.m3u8");
        put("HTV4", "http://live.cdn.mobifonetv.vn/motv/myhtv4_hls.smil/chunklist_b1200000.m3u8");
        put("HTV Thể Thao", "http://live.cdn.mobifonetv.vn/motv/myhtvcthethao_hls.smil/playlist.m3u8");
        put("HTV7", "http://live.cdn.mobifonetv.vn/motv/myhtv7_hls.smil/playlist.m3u8");
        put("HTV9", "http://live.cdn.mobifonetv.vn/motv/myhtv9_hls.smil/playlist.m3u8");
        put("HTV Ca Nhạc", "http://live.cdn.mobifonetv.vn/motv/myhtvccanhac_hls.smil/playlist.m3u8");
        put("HTV Du Lịch", "http://live.cdn.mobifonetv.vn/motv/myhtvcdulich_hls.smil/playlist.m3u8");
        put("HTV Gia Đình", "http://live.cdn.mobifonetv.vn/motv/myhtvcgiadinh_hls.smil/playlist.m3u8");
        put("HTV Phim", "http://live.cdn.mobifonetv.vn/motv/myhtvcphim_hls.smil/playlist.m3u8");
        put("HTV Phụ nữ", "http://live.cdn.mobifonetv.vn/motv/myhtvcphunu_hls.smil/playlist.m3u8");
        put("HTV Thuần Việt", "http://hplusliveall.e96bbe18.sabai.vn/htvcthuanviet-720/playlist.m3u8");

        // Địa phương
        put("ANTV HD", "http://antvlive.ab5c6921.cdnviet.com/antv/chunklist.m3u8");
        put("Quốc Hội HD", "http://1.54.250.40:1935/live/quochoitvlive.stream_720p/playlist.m3u8");
        put("QPVN HD", "http://125.212.132.219:1935/standard/smil:standard.smil/playlist.m3u8");
        put("NHÂN DÂN HD", "http://27.118.16.98:1935/live/truyenhinhnhandan720/chunklist_w2006762604.m3u8");
        put("HÀ NỘI 1 HD", "http://hanoitvlive.8ef55e0c.cdnviet.com/hanoi1.smil/chunklist_b3500000.m3u8");
        put("HÀ NỘI 2 HD", "http://hanoitvlive.8ef55e0c.cdnviet.com/hanoi2.smil/chunklist_b3500000.m3u8");
        put("VĨNH LONG 1", "http://cdn3.vtcplay.vn:1935/VTC/smil:VinhLong1HD.smil/playlist.m3u8");
        put("VĨNH LONG 2", "http://cdn3.vtcplay.vn:1935/VTC/smil:VinhLong2HD.smil/playlist.m3u8");

    }};

    public static final LinkedHashMap<String, Integer> IMAGE_DICT = new LinkedHashMap<String, Integer>()
    {{
        // VTV channels
        put("VTV1", R.drawable.vtv1);
        put("VTV2", R.drawable.vtv2);
        put("VTV3", R.drawable.vtv3);
        put("VTV4", R.drawable.vtv4);
        put("VTV5", R.drawable.vtv5);
        put("VTV6", R.drawable.vtv6);
        put("VTV7", R.drawable.vtv7);
        put("VTV8", R.drawable.vtv8);

    }};

    public static final LinkedHashMap<String, String>  DESC_DICT = new LinkedHashMap<String, String>(){{
        // VTV channels
        put("VTV1", "Kênh thời sự tổng hợp");
        put("VTV2", "Kênh Khoa học và Đời sống");
        put("VTV3", "Kênh Giải trí tổng hợp");
        put("VTV4", "Kênh truyền hình Đối ngoại quốc gia");
        put("VTV5", "Kênh truyền hình tiếng dân tộc");
        put("VTV6", "Kênh truyền hình tương tác dành cho giới trẻ");
        put("VTV7", "Kênh truyền hình Giáo dục quốc gia");
        put("VTV8", "Kênh truyền hình quốc gia hướng vào khu vực Miền Trung và Tây Nguyên");
        put("VTV9", "Kênh truyền hình quốc gia hướng vào khu vực Nam Bộ");

        // VTV Cab
        put("VTVcab1", "Giải Trí TV HD");
        put("VTVcab2", "Phim Việt HD");
        put("VTVcab4", "Văn Hóa HD");
        put("VTVcab5", "E Channel HD");
        put("VTVcab6_", "HayTV HD");
        put("VTVcab7", "D Dramas HD");
        put("VTVcab8", "BiBi");
        put("VTVcab9", "Info TV HD");
        put("VTVcab10", "HD");
        put("VTVcab12", "StyleTV HD");
        put("VTVcab15", "M Channel HD");
        put("VTVcab17", "Yeah1! TV");
        put("VTVcab19", "Film TV");
        put("VTVcab20", "V Family");
        put("VTVcab21", "Cartoon Kids HD");
        put("VTVcab22", "LIFE TV");

        // VTC
        put("VTC1", "Kênh Thời sự - Chính trị - Tổng hợp");
        put("VTC2", "Kênh Truyền hình chuyên biệt về Khoa học - Công nghệ & Đời sống");
        put("VTC3", "Kênh truyền hình chuyên biệt về Thể thao - Văn hóa - Giải trí");
        put("VTC4", "Kênh Thời trang và Cuộc sống");
        put("VTC5", "Kênh Giải trí tổng hợp");
        put("VTC6", "Kênh Phim truyện Việt Nam");
        put("VTC7", "TodayTV ");
        put("VTC8", "Kênh truyền hình chuyên biệt về Kinh tế - Tài chính và Chứng khoán VITV");
        put("VTC9", "Kênh giải trí");
        put("VTC10", "Kênh giải trí tương tác");
        put("VTC11", "Kênh truyền hình dành cho thiếu nhi - KidsTV");
        put("VTC13", "iTV");
        put("VTC14", "Kênh truyền hình chuyên biệt về môi trường");
        put("VTC16", "Kênh truyền hình nông nghiệp - Nông thôn và nông dân");
        put("Vietnam Journey HD", "Du lịch Việt Nam");

        // HTV
        put("HTV1", "HTV1 HD");
        put("HTV2", "HTV2 HD");
        put("HTV3", "HTV3 HD");
        put("HTV4", "HTV4 HD");
        put("HTV Thể Thao", "Kênh truyền hình thể thao");
        put("HTV7", "HTV7 HD");
        put("HTV9", "HTV9 HD");
        put("HTV Ca Nhạc", "Kênh truyền hình ca nhạc");
        put("HTV Du Lịch", "Kênh truyền hình du lịch");
        put("HTV Gia Đình", "Kênh truyền hình gia đình");
        put("HTV Phim", "Kênh truyền hình về phim");
        put("HTV Phụ nữ", "Kênh truyền hình phụ nữ");
        put("HTV Thuần Việt", "Kênh truyền hình thuần Việt");

        // Địa phương
        put("ANTV HD", "An Ninh TV");
        put("Quốc Hội HD", "Quốc hội");
        put("QPVN HD", "Quốc phòng Việt Nam");
        put("NHÂN DÂN HD", "Truyền hình nhân dân");
        put("HÀ NỘI 1 HD", "Kênh Hà Nội 1");
        put("HÀ NỘI 2 HD", "Kênh Hà Nội 2");
        put("VĨNH LONG 1", "Đài truyền hình Vĩnh Long 1");
        put("VĨNH LONG 2", "Đài truyền hình Vĩnh Long 2");

    }};

    public static final Integer NO_IMG = R.drawable.no_img;
    

}
